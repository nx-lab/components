import { DocumentGeneratorAspect } from './document-generator.aspect';

export type { DocumentGeneratorMain } from './document-generator.main.runtime';
export default DocumentGeneratorAspect;
export { DocumentGeneratorAspect };
