import { Aspect } from '@teambit/harmony';

export const DocumentGeneratorAspect = Aspect.create({
  id: 'watheia.nx-lab/aspects/document-generator',
});
  