import React from "react"
import { Grid } from "./grid"
import { View } from "@adobe/react-spectrum"
import { ColorValue } from "@react-types/shared"
import { DarkTheme, LightTheme } from "@watheia/nx-lab.spectrum.theme"
import { repeat } from "lodash"

export const GridLayout_explicit = () => (
  <>
    <DarkTheme>
      <Grid
        data-testid="explicit-grid"
        areas={["header  header", "sidebar content", "footer  footer"]}
        columns={["1fr", "3fr"]}
        rows={["size-1000", "auto", "size-1000"]}
        height="size-6000"
        gap="size-100"
      >
        <View backgroundColor="celery-600" gridArea="header" />
        <View backgroundColor="blue-600" gridArea="sidebar" />
        <View backgroundColor="purple-600" gridArea="content" />
        <View backgroundColor="magenta-600" gridArea="footer" />
      </Grid>
    </DarkTheme>
  </>
)

export const GridLayout_implicit = ({
  colors = ["celery-600", "blue-600", "purple-600", "magenta-600"],
}: {
  colors?: ColorValue[]
}) => (
  <>
    <LightTheme>
      <Grid
        data-testid="implicit-grid"
        columns={repeat("auto-fit", 800)}
        autoRows="size-800"
        justifyContent="center"
        gap="size-100"
      >
        {colors && colors.map((color) => <View key={color} backgroundColor={color} />)}
      </Grid>
    </LightTheme>
  </>
)
