import React from "react"
import { render } from "@testing-library/react"
import { GridLayout_explicit, GridLayout_implicit } from "./grid.composition"

describe("Grid Layout", () => {
  it("should render explicit grid layout", () => {
    const { getByTestId } = render(<GridLayout_explicit />)
    const rendered = getByTestId("explicit-grid")
    expect(rendered).toBeTruthy()
  })

  it("should render implicit grid layout", () => {
    const { getByTestId } = render(<GridLayout_implicit />)
    const rendered = getByTestId("implicit-grid")
    expect(rendered).toBeTruthy()
  })
})
