import React from "react"
import { Flex } from "./flex"
import { View } from "@adobe/react-spectrum"
import { DarkTheme, LightTheme } from "@watheia/nx-lab.spectrum.theme"

export const FlexLayout_vertical = () => (
  <>
    <DarkTheme>
      <Flex
        data-testid="FlexLayout_vertical"
        direction="column"
        width="size-2000"
        height="size-24000"
        gap="size-100"
        margin="size-200"
      >
        <View backgroundColor="celery-600" height="size-800" />
        <View backgroundColor="blue-600" height="size-800" />
        <View backgroundColor="magenta-600" height="size-800" />
      </Flex>
    </DarkTheme>
  </>
)

export const FlexLayout_horizontal = () => (
  <>
    <DarkTheme>
      <Flex
        data-testid="FlexLayout_horizontal"
        direction="column"
        width="size-2000"
        height="size-24000"
        gap="size-100"
        margin="size-200"
      >
        <View backgroundColor="celery-600" height="size-800" />
        <View backgroundColor="blue-600" height="size-800" />
        <View backgroundColor="magenta-600" height="size-800" />
      </Flex>
    </DarkTheme>
  </>
)
