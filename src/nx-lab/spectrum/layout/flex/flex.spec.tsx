import React from "react"
import { render } from "@testing-library/react"
import { FlexLayout_vertical, FlexLayout_horizontal } from "./flex.composition"

it("should render with the FlexLayout_horizontal", () => {
  const { getByTestId } = render(<FlexLayout_horizontal />)
  const rendered = getByTestId("FlexLayout_horizontal")
  expect(rendered).toBeTruthy()
})

it("should render with the FlexLayout_vertical", () => {
  const { getByTestId } = render(<FlexLayout_vertical />)
  const rendered = getByTestId("FlexLayout_vertical")
  expect(rendered).toBeTruthy()
})
