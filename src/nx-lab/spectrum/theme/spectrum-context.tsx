import { createContext } from "react"

export type SpectrumContextType = {
  /**
   * primary color of theme.
   */
  mode?: "light" | "dark"
}

export const SpectrumContext = createContext<SpectrumContextType>({
  mode: "light",
})
