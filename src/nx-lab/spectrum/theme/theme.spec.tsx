import React from "react"
import { DarkTheme, LightTheme } from "./theme.composition"
import { render } from "@testing-library/react"

describe("spectrum/theme", () => {
  it("should render the button using the dark theme", () => {
    const { getByText } = render(
      <DarkTheme>
        <h1>Hello, DarkTheme!</h1>
      </DarkTheme>
    )

    const rendered = getByText("Hello, DarkTheme!")
    expect(rendered).toBeTruthy()
  })

  it("should render the button using the light theme", () => {
    const { getByText } = render(
      <LightTheme>
        <h1>Hello, LightTheme!</h1>
      </LightTheme>
    )
    const rendered = getByText("Hello, LightTheme!")
    expect(rendered).toBeTruthy()
  })
})
