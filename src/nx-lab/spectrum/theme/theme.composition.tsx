import React, { useContext } from "react"
import { SpectrumProvider, SpectrumProviderProps } from "./spectrum-provider"
import { MockComponent } from "./mock-component"
import { cleanSerif } from "@watheia/nx-lab.spectrum.fonts.clean-serif"

export const LightTheme = (props: SpectrumProviderProps) => {
  return (
    <SpectrumProvider mode="light">
      <div className={cleanSerif}>{props.children || <MockComponent />}</div>
    </SpectrumProvider>
  )
}

export const DarkTheme = (props: SpectrumProviderProps) => {
  return <SpectrumProvider mode="dark">{props.children || <MockComponent />}</SpectrumProvider>
}
