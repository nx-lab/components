import React, { ReactNode } from "react"
import { SpectrumContext } from "./spectrum-context"
import { Provider, defaultTheme } from "@adobe/react-spectrum"

export type SpectrumProviderProps = {
  /**
   * primary color of theme.
   */
  mode?: "light" | "dark"

  /**
   * children to be rendered within this theme.
   */
  children: ReactNode
}

export function SpectrumProvider({ mode, children }: SpectrumProviderProps) {
  return (
    <SpectrumContext.Provider value={{ mode }}>
      <Provider colorScheme={mode} theme={defaultTheme}>
        {children}
      </Provider>
    </SpectrumContext.Provider>
  )
}
