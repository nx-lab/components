import React from "react"
import { Flex, View } from "@adobe/react-spectrum"

/**
 *
 * @returns A simple example component utilizing spectrum design system
 */
export const MockComponent = () => {
  return (
    <Flex
      direction="column"
      width="size-2000"
      gap="size-100"
      data-testid="mock-component"
      margin="size-200"
    >
      <View backgroundColor="celery-600" height="size-800" />
      <View backgroundColor="blue-600" height="size-800" />
      <View backgroundColor="magenta-600" height="size-800" />
    </Flex>
  )
}
