import React from "react"
import { render } from "@testing-library/react"
import { FooterComposition } from "./footer.composition"

it("should render with the correct text", () => {
  const { getByText } = render(<FooterComposition />)
  const rendered = getByText("hello from Footer")
  expect(rendered).toBeTruthy()
})
