import React from "react"
import { Footer } from "./footer"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"

export const FooterComposition = () => (
  <LightTheme>
    <Footer>hello from Footer</Footer>
  </LightTheme>
)
