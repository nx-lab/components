import React from "react"
import { Content } from "./content"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"

export const BasicContent = () => (
  <LightTheme>
    <Content>hello from Content</Content>
  </LightTheme>
)
