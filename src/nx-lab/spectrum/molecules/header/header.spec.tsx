import React from "react"
import { render } from "@testing-library/react"
import { HeaderComposition } from "./header.composition"

it("should render with the correct text", () => {
  const { getByText } = render(<HeaderComposition />)
  const rendered = getByText("hello from Header")
  expect(rendered).toBeTruthy()
})
