import React from "react"
import { Header } from "./header"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"

export const HeaderComposition = () => (
  <LightTheme>
    <Header>hello from Header</Header>
  </LightTheme>
)
