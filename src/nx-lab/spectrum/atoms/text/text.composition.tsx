import React from "react"
import { Text } from "./text"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"

export const TextComposition = () => (
  <LightTheme>
    <Text>hello from Text</Text>
  </LightTheme>
)
