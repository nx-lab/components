import React from "react"
import { render } from "@testing-library/react"
import { BasicDivider } from "./divider.composition"

it("should render with the correct text", () => {
  const { getByTestId } = render(<BasicDivider />)
  const rendered = getByTestId("basic-divider")
  expect(rendered).toBeTruthy()
})
