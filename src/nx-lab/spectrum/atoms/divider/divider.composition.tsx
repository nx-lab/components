import React from "react"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"
import Flex from "@watheia/nx-lab.spectrum.layout.flex"
import Text from "@watheia/nx-lab.spectrum.atoms.text"
import { Divider } from "./divider"

export const BasicDivider = () => (
  <LightTheme>
    <Flex data-testid="basic-divider" direction="column" gap="size-125">
      <Text>Content above</Text>
      <Divider />
      <Text>Content below</Text>
    </Flex>
  </LightTheme>
)
