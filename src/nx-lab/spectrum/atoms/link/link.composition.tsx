import React from "react"
import { Link } from "./link"

export const BasicLink = () => (
  <Link>
    <a href="https://watheia.io/" target="_blank">
      Watheia Labs
    </a>
  </Link>
)
