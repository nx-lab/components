import React from "react"
import { ThemeCompositions } from "@teambit/documenter.theme.theme-compositions"
import { MDXLayout } from "@teambit/ui.mdx-layout"
import { SpectrumDesign } from "./index"

export const SpectrumDesignDocument = () => (
  <ThemeCompositions>
    <MDXLayout>
      <SpectrumDesign />
    </MDXLayout>
  </ThemeCompositions>
)
