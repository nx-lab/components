import React from "react"
import { LightTheme } from "@watheia/nx-lab.spectrum.theme"
import { MDXLayout } from "@teambit/ui.mdx-layout"
import _2021_06Milestone from "./2021-06.milestone.mdx"
import { cleanSerif } from "@watheia/nx-lab.spectrum.fonts.clean-serif"

export const NotesDocument = () => (
  <LightTheme>
    <_2021_06Milestone />
  </LightTheme>
)
