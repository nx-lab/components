import React from "react"

import { LightTheme } from "@watheia/nx-lab.spectrum.theme"
import { MDXLayout } from "@teambit/ui.mdx-layout"
import { Charter } from "."
import { cleanSerif } from "@watheia/nx-lab.spectrum.fonts.clean-serif"

export const CharterDocument = () => (
  <LightTheme>
    <MDXLayout>
      <Charter className={cleanSerif} />
    </MDXLayout>
  </LightTheme>
)
